terraform {
  required_version = "~> 1.1"
}



resource "null_resource" "wait" {
  provisioner "local-exec" {
    command = "sleep 1"
  }
}
resource "null_resource" "printenv22" {
  provisioner "local-exec" {
    command = "env"
  }
}

